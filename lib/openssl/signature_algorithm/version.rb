# frozen_string_literal: true

module OpenSSL
  module SignatureAlgorithm
    VERSION = "1.0.0"
  end
end
